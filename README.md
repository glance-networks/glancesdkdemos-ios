# GlanceSDKDemos-iOS

Sample demo projects for Glance SDK on iOS

This project contains the latest iOS SDKs and samples.  

[Release Notes](https://gitlab.com/glance-networks/glancesdkdemos-ios/-/blob/master/ReleaseNotes.md)
