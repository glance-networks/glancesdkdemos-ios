//
//  ViewController.m
//  TwoWayVideoDemo_objc
//
//  Created by Ankit Desai on 1/23/23.
//

#import "ViewController.h"
#import <Glance_iOSVideo/Glance_iOS.h>

@interface ViewController () <GlanceVisitorDelegate>
@property (weak, nonatomic) IBOutlet UIButton *sessionStartButton;
@property (weak, nonatomic) IBOutlet UILabel *sessionKeyLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [GlanceVisitor addDelegate:self];
    [GlanceVisitor addMaskedView:self.sessionKeyLabel];
}

- (IBAction)startSessionClicked:(id)sender {
    GlanceStartParams *startParams = [[GlanceStartParams alloc] init];
    startParams.key = @"GLANCE_KEYTYPE_RANDOM";
    startParams.video = VideoOff;
    [GlanceVisitor startSession:startParams delegate:nil];
}

-(void)updateLabel:(NSString*)sessionkey {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.sessionKeyLabel.text = sessionkey;
    });
}

-(void)hideShowButton:(int)alpha{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.sessionStartButton.alpha = alpha;
    });
}

- (void)glanceVisitorEvent:(GlanceEvent *)event {
    switch(event.code) {
        case EventVisitorInitialized:
            NSLog(@"EventVisitorInitialized");
            break;
        case EventConnectedToSession:
            [self hideShowButton:0];
            [self updateLabel:event.properties[@"sessionkey"]];
            break;
        case EventSessionEnded:
            [self updateLabel:event.properties[@""]];
            [self hideShowButton:1];
            break;
        default:
            break;
    }
}

@end
