//
//  Glance.swift
//  TwoWayVideoDemo_swiftui
//
//  Created by Ankit Desai on 1/10/23.
//

import Foundation
import Glance_iOSVideo

let GLANCE_GROUP_ID : Int32 = 21489
let GLANCE_SESSION_KEY = "GLANCE_KEYTYPE_RANDOM"

class Glance : NSObject, ObservableObject, GlanceVisitorDelegate {
    static let shared = Glance()
    
    var inSession = false
    
    func initVisitor() {
        GlanceVisitor.initVisitor(GLANCE_GROUP_ID, token: "", name: "", email: "", phone: "")
        
        GlanceVisitor.add(self)
        
        print("Version: " + GlanceSettings().get("version"))
    }
    
    func startSession() {
        if (inSession) {
            return
        }
        let startParams = GlanceStartParams()
        if let startParams = startParams {
            startParams.key = GLANCE_SESSION_KEY
            startParams.video = VideoOff
            GlanceVisitor.startSession(startParams, delegate: nil)
        }
    }
    
    func addMaskedView(view:UIView) {
        GlanceVisitor.addMaskedView(view)
    }
    
    func glanceVisitorEvent(_ event: GlanceEvent!) {
        switch event.code {
        case EventConnectedToSession:
            print("EventConnectedToSession", event.properties["sessionkey"] ?? "")
            inSession = true
            break
        case EventSessionEnded:
            inSession = false
        default:
            break
        }
    }
}
