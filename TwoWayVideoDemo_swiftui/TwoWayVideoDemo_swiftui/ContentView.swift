//
//  ContentView.swift
//  TwoWayVideoDemo_swiftui
//
//  Created by Ankit Desai on 1/10/23.
//

import SwiftUI

struct ContentView: View {
    @State var text = NSMutableAttributedString(string: "Hello World!")
    var body: some View {
        VStack {
            Spacer()
            Button(action: Glance.shared.startSession) {
                Text("Start Session")
            }
            Spacer()
            CustomTextView(text: $text).frame(width: 100, height: 25)
        }
        .padding()
    }
}

struct CustomTextView : UIViewRepresentable {
    @Binding var text: NSMutableAttributedString

    func makeUIView(context: Context) -> UITextView {
        UITextView()
    }

    func updateUIView(_ uiView: UITextView, context: Context) {
        Glance.shared.addMaskedView(view: uiView)
        uiView.attributedText = text
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
