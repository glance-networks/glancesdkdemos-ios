//
//  TwoWayVideoDemo_swiftuiApp.swift
//  TwoWayVideoDemo_swiftui
//
//  Created by Ankit Desai on 1/10/23.
//

import SwiftUI

@main
struct TwoWayVideoDemo_swiftuiApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
    
    init() {
        Glance.shared.initVisitor()
    }
}
